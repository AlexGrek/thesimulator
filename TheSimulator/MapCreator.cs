﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator
{
    public static class MapCreator
    {
        public static void CreateFromStrings(string[] strs)
        {
            for (int i = 0; i < strs.Length && i < Map.Height; i++)
            {
                var s = strs[i];
                for (int j = 0; j < s.Length && j < Map.Width; j++)
                {
                    if (s[j] != ' ' && s[j] != '.')
                        Map.PlaceObject(CreateObject(s[j], j, i));
                }
            }
        }

        public static MapObject CreateObject(char symbol, int x, int y)
        {
            MapObject mapobj = null;
            Vector2 position = new Vector2(x, y);

            switch (symbol)
            {
                case '#':
                    mapobj = new MapObjects.Wall(position);
                    break;
                case 'p':
                    mapobj = new MapObjects.Plankton(position);
                    break;
                case 'S':
                    mapobj = new MapObjects.Seeker(position);
                    break;
                case 'H':
                    mapobj = new MapObjects.Hunter(position);
                    break;
            }

            //add actor to actors list
            if (mapobj is Actor)
                Map.Current.Actors.Add((Actor)mapobj);

            return mapobj;
        }
    }
}
