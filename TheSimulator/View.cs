﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TheSimulator
{
    class View
    {
        private object _sync = new object();

        public void HandleMapChanges(IEnumerable<Vector2> affected)
        {
            lock (_sync)
            {
                foreach (var pos in affected)
                {
                    Console.SetCursorPosition(pos.X, pos.Y);
                    var target = Map.Current[pos.X, pos.Y];
                    Console.Write(target == null ? ' ' : target.Ch);
                } 
                
            }
        }

        public void DrawAllMap()
        {
            Console.Clear();
            for (int i = 0;  i < Map.Height; i++)
            {
                for (int j = 0; j < Map.Width; j++)
                {
                    if (Map.Current[j, i] != null)
                    {
                        Console.SetCursorPosition(j, i);
                        Console.Write(Map.Current[j, i].Ch);
                    }
                        
                }
            }
        }

        public static void Mark(Vector2 pos, string what)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.SetCursorPosition(pos.X, pos.Y);
                    Console.Write(what);
            Console.ForegroundColor = ConsoleColor.White;
            Thread.Sleep(100);
        }
    }
}
