﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator
{
    /// <summary>
    /// Something on the map
    /// </summary>
    public abstract class MapObject
    {
        public Vector2 Position { get; internal set; }
        public abstract char Ch { get; }    //drawable character

        public MapObject(Vector2 pos)
        {
            Position = pos;
        }

        public int X { get { return Position.X; } }
        public int Y { get { return Position.Y; } }

    }
}
