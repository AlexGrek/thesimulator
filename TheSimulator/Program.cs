﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using ConsoleMenu;

namespace TheSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var maplines = File.ReadAllLines(SelectFileMenu());

            MapCreator.CreateFromStrings(maplines);
            //Map.Current.RunActorThreads();

            

            var view = new View();
            view.DrawAllMap();
            Map.MapChangedEvent += view.HandleMapChanges;
            Map.ChangeConcurrentMode(false);
            Map.Current.RunActorsSyncForever(40, 16);

            Console.ReadKey();
        }

        private static string SelectFileMenu()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.txt");
            var filenames = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
                filenames[i] = Path.GetFileNameWithoutExtension(files[i]);
            var menu = new FullscreenSelector(filenames, "Select file to load:");
            return files[menu.Show()];
        }
    }
}
