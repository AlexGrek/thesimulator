﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator
{
    public struct Vector2
    {
        public  int X { get; }
        public  int Y { get; }

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Vector2(Vector2 vector) : this(vector.X, vector.Y) { }

        #region Operators
        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2 operator *(Vector2 a, int b)
        {
            return new Vector2(a.X * b, a.Y * b);
        }

        public static Vector2 operator /(Vector2 a, int b)
        {
            return new Vector2(a.X / b, a.Y / b);
        }

        #endregion

        #region Distance
        public static double SquareDistance(Vector2 a, Vector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static double Distance(Vector2 a, Vector2 b)
        {
            return Math.Sqrt(a.X * b.X + a.Y * b.Y);
        }

        public double Distance(Vector2 b)
        {
            var dx = this.X - b.X;
            var dy = this.Y - b.Y;
            return Math.Sqrt((dx*dx) + (dy*dy));
        }

        public double SquareDistance(Vector2 b)
        {
            return X * b.X + Y * b.Y;
        }

        public static Vector2 operator *(Vector2 a, double b)
        {
            return new Vector2((int)Math.Round(a.X * b), (int)Math.Round(a.Y * b));
        }

        public static bool operator ==(Vector2 left, Vector2 right)
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator !=(Vector2 left, Vector2 right)
        {
            return left.X != right.X || left.Y != right.Y;
        }

        public override bool Equals(object obj)
        {
            var right = obj as Vector2?;
            return right != null && X == right.Value.X && Y == right.Value.Y;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ (Y.GetHashCode() >> (X.GetHashCode() % 30));
        }
        #endregion

        public Vector2 DirectionTo(Vector2 target)
        {
            var dir = target - this;
            return new Vector2(Math.Sign(dir.X), Math.Sign(dir.Y));
        }

        public bool IsZero()
        {
            return X == 0 && Y == 0;
        }

        public double Lenght()
        {
            throw new NotImplementedException();
        }

        public Vector2 OnlyX()
        {
            return new Vector2(X, 0);
        }

        public Vector2 OnlyY()
        {
            return new Vector2(0, Y);
        }

        public Vector2[] Surroundings()
        {
            return new Vector2[4]
            {
                new Vector2(X + 1, Y),
                new Vector2(X, Y - 1),
                new Vector2(X - 1, Y),
                new Vector2(X, Y + 1)
            };
        }

        public bool IsInBounds(int x, int y)
        {
            return this.X >= 0 && this.Y >= 0 && this.X < x && this.Y < y;
        }

        public bool IsInBounds<T>(T[,] array)
        {
            return IsInBounds(array.GetLength(0), array.GetLength(1));
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
