﻿//#define HARDCOREDEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace TheSimulator
{


    /// <summary>
    /// Represents MapObject, that can perform actions
    /// </summary>
    public abstract class Actor : MovableMapObject
    {
        internal int Counter;

        public Flag ActorFlag { get; internal set; }

        public bool IsAlive { get; internal set; } = true;

        public Actor(Vector2 pos) : base(pos)  { }

        public Thread PersonalThread { get; internal set; }

        public void AssignThread(Thread t)
        {
            PersonalThread = t;
        }

        public void Die()
        {
            IsAlive = false;
            //View.Mark(this.Position, "!");
            if (PersonalThread != null)
            {
                PersonalThread.Abort();
            }
            Map.Forget(this);
        }

        /// <summary>
        /// Core method of every living being
        /// (thread-based concurrency)
        /// </summary>
        public abstract void ActForever();

        //  Do something!
        public abstract void Act();

        public abstract void ActOnTick(int tick);

        public static Vector2 RandomMoveAnywhere()
        {
            Random r = Map.Current.Rand;
            var delta = r.Next(3) - 1;
            Vector2 movement;
            if (r.Next() % 2 > 0)
                movement = new Vector2(0, delta);
            else
                movement = new Vector2(delta, 0);
            return movement;
        }

        public Vector2 MoveToTargetDirectly(Vector2 target)
        {
            Random r = Map.Current.Rand;
            Vector2 movement;
            var dir = Position.DirectionTo(target);

            //don't move if target is reached
            if (dir.X == 0 && dir.Y == 0) 
                return new Vector2(); //(0, 0)

            //one-directional move is only possible
            if (dir.X == 0 || dir.Y == 0)
            {
                if (Map.Current[Position + dir] == null)
                {
                    return dir;
                }
                else return new Vector2(); //(0, 0)
            }

            //should move in both directions
            if (Map.IsPositionEmpty(Position + dir.OnlyX()))
            {
                if (Map.IsPositionEmpty(Position + dir.OnlyY()))
                {
                    //can move in both directions, so randomize
                    if (r.Next() % 2 == 0)
                        movement = dir.OnlyX();
                    else
                        movement = dir.OnlyY();
                    return movement;
                }
                //only horizontal move possible
                return dir.OnlyX();
            }else
            {
                //only vertical move possible
                if (Map.IsPositionEmpty(Position + dir.OnlyY()))
                {
                   return dir.OnlyY();
                }
                else
                {
                    //no possible moves
                    return new Vector2(); //(0, 0)
                }
            }
        }

        public Stack<Vector2> PathfindAstar(Vector2 target)
        {
            var toVisit = new SortedList<Vector2, int>(new Pathfind.VectorHeuristicsComparer(target));
            toVisit.Add(Position, 0);
            bool found = false;
            if (target == Position)
                return null;
#if HARDCOREDEBUG
            View.Mark(new Vector2(), target.ToString());
#endif
            int i = 0;
            int[,] visited = new int[Map.Width, Map.Height];
            visited[Position.X, Position.Y] = 1;
            while (toVisit.Count > 0 && !found) //while there are unvisited cells
            {
                i++;
                var next = toVisit.Keys[0];
                if (next != target)
                {
                    var currentValue = visited[next.X, next.Y];
                    toVisit.RemoveAt(0);
                    foreach (var nextToSee in next.Surroundings())
                    {
                        if (nextToSee == target)
                        {
                            visited[nextToSee.X, nextToSee.Y] = currentValue + 1;
                            next = target;
                            found = true;
#if HARDCOREDEBUG
                            View.Mark(next, "Got IT!");
#endif
                            break;
                        }
                            

                        if (
                            (Map.IsPositionEmpty(nextToSee) && //position can be visited
                            visited[nextToSee.X, nextToSee.Y] == 0 && //position is not visited yet
                            !toVisit.ContainsKey(nextToSee))  //position is not already marked to visit
                            )
                        {
                            toVisit.Add(nextToSee, i);
                            visited[nextToSee.X, nextToSee.Y] = currentValue + 1;
#if HARDCOREDEBUG
                            View.Mark(nextToSee, visited[nextToSee.X, nextToSee.Y].ToString());
#endif
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            var current = target;
            Stack<Vector2> positions = new Stack<Vector2>();

            if (!found)
                return null;

            while (current != Position)
            {
                positions.Push(current);

#if HARDCOREDEBUG
                View.Mark(current, "@");
#endif

                //find closest way to move back
                int min = visited[current.X, current.Y];
                Vector2 minV = current;
                foreach (var variant in current.Surroundings())
                {
                    if (variant == Position)
                    {
                        minV = variant;
                        break;
                    }

                    if (variant.IsInBounds(visited) && visited[variant.X, variant.Y] > 0 && visited[variant.X, variant.Y] < min)
                    {
                            min = visited[variant.X, variant.Y];
                            minV = variant;
                    }
                }

                current = minV;
            }

            return positions;
        }
    }

    [Flags]
    public enum Flag { Observer, Food, Predator }
}
