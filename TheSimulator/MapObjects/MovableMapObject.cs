﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator
{
    /// <summary>
    /// MapObject that can be moved, by itself or by another objects
    /// </summary>
    public abstract class MovableMapObject : MapObject

    {

        public MovableMapObject(Vector2 pos) : base(pos)  { }

        /// <summary>
        /// Change the position to the vector
        /// </summary>
        /// <param name="there">New position</param>
        public void Place(Vector2 there)
        {
            Position = there;
        }

        /// <summary>
        /// Change the position by the vector
        /// </summary>
        /// <param name="vector">Position modifier</param>
        public void Move(Vector2 vector)
        {
            Map.MoveObject(Position, Position + vector);
            Position = Position + vector;
        }

        public bool TryMove(Vector2 vector)
        {
            if (Map.TryMoveObject(Position, Position + vector))
            {
                Position = Position + vector;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Change the position by the vector (scaled)
        /// </summary>
        /// <param name="vector">Position modifier</param>
        /// <param name="scale">Vector multiplier</param>
        public void Move(Vector2 vector, double scale)
        {
            Move(vector * scale);
        }
    }
}
