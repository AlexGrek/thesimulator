﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator.MapObjects
{
    /// <summary>
    /// MapObject that cannot do anything without help
    /// </summary>
    public abstract class Obstacle : MapObject
    {
        public Obstacle(Vector2 pos) : base(pos)  {   }
    }
}
