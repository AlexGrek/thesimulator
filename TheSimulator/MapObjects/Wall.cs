﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator.MapObjects
{
    public class Wall : Obstacle
    {
        public override char Ch { get { return '#'; } }

        public Wall(Vector2 pos) : base(pos)  { }
    }
}
