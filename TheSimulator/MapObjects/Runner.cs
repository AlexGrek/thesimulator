﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TheSimulator.MapObjects
{
    class Runner : Actor
    {
        public Runner(Vector2 pos) : base(pos) {
            ActorFlag = Flag.Food;
        }
        int _internalCounter = 0;

        Vector2 _target = new Vector2(Map.Width / 2, Map.Height / 2);

        bool _targetExists;
        int _feed;

        public override char Ch
        {
            get
            {
                return 'r';
            }
        }

        public override void ActForever()
        {
            for (;;)
            {
                Thread.Sleep(70 + _feed);
                Act();
            }
        }

        public override void Act()
        {
            _internalCounter++;
            lock (Map.Current.Sync)
            {
                var vector = _targetExists ? MoveToTargetDirectly(_target) : RandomMoveAnywhere();
                if (vector.IsZero())
                    vector = RandomMoveAnywhere();
                TryMove(vector);
                var lim = Map.Current.Actors.Count;
                _feed--;
                if (_internalCounter % 8 == 0 || _target.Distance(Position) < 4)
                {
                    var target = Map.FindClosestActorOfType<MapObjects.Plankton>(this.Position);
                    _targetExists = true;
                    if (target != null)
                        _target = target.Position;
                    else
                        _targetExists = false;
                }
            }
        }

        /// <summary>
        /// Make a child
        /// </summary>
        public void Replicate()
        {
            //find a place for our child
            var randomDirection = RandomMoveAnywhere();
            if (randomDirection.IsZero() || Map.Current.Actors.Count > 100)
                return;
            if (Map.IsPositionEmpty(Position + randomDirection))
            {
                //place found, so let's make children!
                Map.Current.TryAddActorAndRun(new Runner(Position + randomDirection));
                _feed = 0;
            }
        }

        public override void ActOnTick(int tick)
        {
            Counter += tick;
            if (Counter > 45)
            {
                Act();
                Counter = 0;
            }
        }
    }
}
