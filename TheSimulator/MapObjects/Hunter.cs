﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TheSimulator.MapObjects
{
    class Hunter : Actor
    {
        public Hunter(Vector2 pos) : base(pos)  { }

        Vector2 _target = new Vector2(Map.Width / 2, Map.Height / 2);
        int _innerCounter = 0;

        Stack<Vector2> path = new Stack<Vector2>();

        bool _targetExists;
        int _feed;

        public override char Ch
        {
            get
            {
                return 'H';
            }
        }

        public override void ActForever()
        {
            
            for (;;)
            {
                Thread.Sleep(500);
                Act();
            }
        }

        public void EatPlankton()
        {
            var plankton = Map.Current[_target] as Plankton;
            if (_target.Distance(Position) < 2 && plankton != null)
            {
                plankton.Die();
                _feed += Map.Current.Rand.Next(70);
                if (_feed > 300)
                    _feed = 300;
            }
        }

        public override void Act()
        {
            _innerCounter++;
            lock (Map.Current.Sync)
            {
                var vector = new Vector2(0, 0);
                if (path != null && path.Count < 2)
                    vector = _targetExists ? MoveToTargetDirectly(_target) : RandomMoveAnywhere();
                if (path == null || path.Count < 1)
                    path = this.PathfindAstar(_target);
                if (path != null)
                    vector = Position.DirectionTo(path.Pop());
                if (vector.IsZero())
                    vector = RandomMoveAnywhere();
                TryMove(vector);
                if (_targetExists)
                    EatPlankton();
                var lim = Map.Current.Actors.Count;
                _feed = _feed > lim ? _feed - lim / 2 : _feed;
                if (_innerCounter % 2 == 0 || _target.Distance(Position) < 4 || (path != null && path.Count < 2))
                {
                    var target = Map.FindClosestActorOfType<MapObjects.Plankton>(this.Position);
                    _targetExists = true;
                    if (target != null)
                        _target = target.Position;
                    else
                        _targetExists = false;
                }
            }
        }

        public override void ActOnTick(int tick)
        {
            Counter += tick;
            if (Counter > 35)
            {
                Act();
                Counter = 0;
            }
        }
    }
}
