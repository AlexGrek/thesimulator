﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography;

namespace TheSimulator.MapObjects
{
    public class Plankton : Actor
    {
        int _activity;

        public Plankton(Vector2 pos) : base(pos)  {
            _activity = Map.Current.Rand.Next(20);
            ActorFlag = Flag.Food;
        }

        int _additionalSleepTime = 0;
        int _replicationReadyPercents = 0;

        public override char Ch
        {
            get
            {
                return 'p';
            }
        }

        public override void ActForever()
        {
            for (;;)
            {
                Thread.Sleep(80 + _additionalSleepTime);
                lock (Map.Current.Sync)
                {
                    Act();
                }
            }
        }

        /// <summary>
        /// Make a child
        /// </summary>
        public void Replicate()
        {
            //find a place for our child
            var randomDirection = RandomMoveAnywhere();
            if (randomDirection.IsZero() || Map.Current.Actors.Count > 100)
                return;
            if (Map.IsPositionEmpty(Position + randomDirection))
            {
                //place found, so let's make children!
                Map.Current.TryAddActorAndRun(new Plankton(Position + randomDirection));
                _replicationReadyPercents = 0;
            }
        }

        public override void Act()
        {
            var vector = RandomMoveAnywhere();
            Random r = new Random();
            _additionalSleepTime = r.Next(400);
            TryMove(vector);
            if (_replicationReadyPercents >= 100)
                Replicate();
            else
                _replicationReadyPercents += Map.Current.Rand.Next(25);
        }

        public override void ActOnTick(int tick)
        {
            Counter += tick + _activity;
            if (Counter > 80)
            {
                Act();
                Counter = 0;
            }
        }
    }
}
