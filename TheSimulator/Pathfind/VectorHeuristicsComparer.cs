﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator.Pathfind
{
    public class VectorHeuristicsComparer : IComparer<Vector2>
    {
        Vector2 _target;

        public VectorHeuristicsComparer(Vector2 target)
        {
            _target = target;
        }

        public int Compare(Vector2 x, Vector2 y)
        {
            return Math.Sign(x.Distance(_target) -y.Distance(_target));
        }
    }
}
