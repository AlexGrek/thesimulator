﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TheSimulator
{
    /// <summary>
    /// Singleton class that represents the world itself as a 2D matrix of MapObjects
    /// </summary>
    class Map
    {
        public static Boolean ConcurrentMode { get; internal set; }

        public Queue<Actor> DoomedActors = new Queue<Actor>();
        public Queue<Actor> NewbornActors = new Queue<Actor>();

        public static readonly int Height = 24, Width = 80;

        private static volatile Map _current;
        private static Object _sync = new object(); //for singleton lock
        private static Object _mut = new object();  //for matrix modification
        public object Sync { get; private set; } = new object();

        private MapObject[,] matrix = new MapObject[Width, Height];
        public List<Actor> Actors { get; private set; } = new List<Actor>();

        public Random Rand { get; private set; } = new Random();

        private Map() { }

        public delegate void MapChangedEventHandler(IEnumerable<Vector2> affected);

        public static event MapChangedEventHandler MapChangedEvent;

        public static Map Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_sync)
                        if (_current == null)
                            _current = new Map();
                }

                return _current;
            }
        }

        /// <summary>
        /// Remove actor from map safely
        /// </summary>
        /// <param name="a"></param>
        public static void Forget(Actor a)
        {
            RemoveObject(a.Position);
        }

        /// <summary>
        /// Can be used for making MapObjects alive
        /// </summary>
        /// <param name="a"></param>
        public static void AddToActors(Actor a)
        {
            if (!ConcurrentMode)
                Current.NewbornActors.Enqueue(a);
            else
                Current.Actors.Add(a);
        }

        /// <summary>
        /// Never use this method after the start!
        /// </summary>
        /// <param name="to"></param>
        public static void ChangeConcurrentMode(bool to)
        {
            ConcurrentMode = to;
        }

        public MapObject this[int x, int y]
        {
            get
            {
                if (IsPositionValid(x, y))
                    return Current.matrix[x, y];
                else return new MapObjects.Wall(new Vector2(x, y));
            }
        }
        public MapObject this[Vector2 vector]
        {
            get
            {
                if (IsPositionValid(vector))
                    return Current.matrix[vector.X, vector.Y];
                else return new MapObjects.Wall(vector);
            }
        }

        public void RunActorThreads()
        {
            foreach (var actor in Actors)
            {
                var actorThread = new Thread(() => actor.ActForever());
                actor.AssignThread(actorThread);
            }

            foreach (var actor in Actors)
            {
                actor.PersonalThread.Start();
            }
        }

        public void RunActorsSyncForever(int delay, int tick)
        {
            var timer = new System.Diagnostics.Stopwatch();
            for (;;)
            {
                timer.Restart();
                foreach (var actor in Actors)
                {
                    //timer.Restart();
                    if (actor.IsAlive)
                        actor.ActOnTick(tick);
                    /*timer.Stop();
                    if (timer.ElapsedMilliseconds > 26)
                    {
                        View.Mark(new Vector2(0, 0), $"Too long: {actor.ToString()}: {timer.ElapsedMilliseconds}");
                        Thread.Sleep(1000);
                    }*/
                }
                while (DoomedActors.Count > 0)
                    Actors.Remove(DoomedActors.Dequeue());
                while (NewbornActors.Count > 0)
                    Actors.Add(NewbornActors.Dequeue());

                timer.Stop();
                //View.Mark(new Vector2(0, 0), $" {timer.ElapsedMilliseconds}   ");

                if (timer.ElapsedMilliseconds < delay)
                    Thread.Sleep(delay - (int)timer.ElapsedMilliseconds);
            }
        }

        /// <summary>
        /// Add actor to map and run it's ActForever() thread
        /// </summary>
        /// <param name="a">Actor to add</param>
        /// <returns>true if added, false if this place is not vacant</returns>
        public bool TryAddActorAndRun(Actor a)
        {
            lock (_mut)
            {
                if (TryPlaceObject(a) == true)
                {
                    if (ConcurrentMode)
                    {
                        Actors.Add(a);
                        a.PersonalThread = new Thread(() => a.ActForever());
                    }
                    else
                    {
                        Current.NewbornActors.Enqueue(a);
                    }
                }
                else return false;
            }
            if (ConcurrentMode)
                a.PersonalThread.Start();
            return true;
        }

        public static void MoveObject(Vector2 oldPosition, Vector2 newPosition)
        {
            if (oldPosition == newPosition)
                return;
            lock (_mut)
            {
                var obj = Current.matrix[oldPosition.X, oldPosition.Y];
                Current.matrix[oldPosition.X, oldPosition.Y] = null;
                Current.matrix[newPosition.X, newPosition.Y] = obj;
            }

            //raise Map Changed Event
            if (MapChangedEvent != null)
                MapChangedEvent(new Vector2[2] { oldPosition, newPosition });
        }

        public static void SwapObjects(Vector2 aPosition, Vector2 bPosition)
        {
            if (aPosition == bPosition)
                return;
            lock (_mut)
            {
                var obj1 = Current.matrix[aPosition.X, aPosition.Y];
                Current.matrix[aPosition.X, aPosition.Y] = Current.matrix[aPosition.X, aPosition.Y];
                Current.matrix[bPosition.X, bPosition.Y] = obj1;
            }

            //raise Map Changed Event
            if (MapChangedEvent != null)
                MapChangedEvent(new Vector2[2] { aPosition, bPosition });
        }

        public static void PlaceObject(MapObject obj)
        {
            lock (_mut)
            {
                if (obj != null)
                    Current.matrix[obj.X, obj.Y] = obj;
            }

            //raise Map Changed Event
            if (MapChangedEvent != null)
                MapChangedEvent(new Vector2[1] { obj.Position });
        }

        public static bool TryPlaceObject(MapObject obj)
        {
            lock (_mut)
            {
                if (obj.X < Width && obj.Y < Height && Current.matrix[obj.X, obj.Y] == null)
                {
                    Current.matrix[obj.X, obj.Y] = obj;

                    //raise Map Changed Event
                    if (MapChangedEvent != null)
                        MapChangedEvent(new Vector2[1] { obj.Position });

                    return true;
                }
                return false;
            }
        }

        public static bool TryMoveObject(Vector2 oldPosition, Vector2 newPosition)
        {
            if (oldPosition == newPosition)
                return true;
            lock (_mut)
            {
                if (IsPositionValid(newPosition) && Current.matrix[newPosition.X, newPosition.Y] == null)
                {
                    var obj = Current.matrix[oldPosition.X, oldPosition.Y];
                    Current.matrix[oldPosition.X, oldPosition.Y] = null;
                    Current.matrix[newPosition.X, newPosition.Y] = obj;

                    //raise Map Changed Event
                    if (MapChangedEvent != null)
                        MapChangedEvent(new Vector2[2] { oldPosition, newPosition });

                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if position is valid and with no
        /// MapObjects in it
        /// </summary>
        /// <param name="position">Position to check</param>
        /// <returns>True if empty, false if not empty or invalid</returns>
        public static bool IsPositionEmpty(Vector2 position)
        {
            if (IsPositionValid(position) && Current.matrix[position.X, position.Y] == null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if position is inside the map
        /// </summary>
        /// <param name="position">Position to check</param>
        /// <returns>True if valid, false if not</returns>
        public static bool IsPositionValid(Vector2 position)
        {
            if (position.X < Width && position.Y < Height && position.X >= 0 && position.Y >= 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if position is inside the map
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <returns>True if valid, false if not</returns>
        public static bool IsPositionValid(int x, int y)
        {
            if (x < Width && y < Height && x >= 0 && y >= 0)
            {
                return true;
            }
            return false;
        }


        public static T FindClosestOfType<T>(Vector2 from) where T : class
        {
            MapObject closest = null;
            //todo: optimize
            lock (_mut)
            foreach (var thing in Current.matrix)
                {
                    if (thing != null && thing is T)
                    {
                        if (closest != null &&
                            from.Distance(thing.Position) < from.Distance(closest.Position))
                            closest = thing;
                        if (closest == null)
                            closest = thing;
                    }
                }


            if (closest == null)
                return null;
            else return closest as T;
        }

        public static T FindClosestActorOfType<T>(Vector2 from) where T : class
        {
            Actor closest = null;
            lock (_mut)
            foreach (var thing in Current.Actors)
                {
                    if (thing != null && thing is T)
                    {
                        if (closest != null &&
                            from.Distance(thing.Position) < from.Distance(closest.Position))
                            closest = thing;
                        if (closest == null)
                            closest = thing;
                    }
                }


            if (closest == null)
                return null;
            else return closest as T;
        }

        public static Actor FindClosestActorWithFlag(Vector2 from, Flag flag) 
        {
            Actor closest = null;
            lock (_mut)
                foreach (var thing in Current.Actors)
                {
                    if (thing != null && thing.ActorFlag == flag)
                    {
                        if (closest != null &&
                            from.Distance(thing.Position) < from.Distance(closest.Position))
                            closest = thing;
                        if (closest == null)
                            closest = thing;
                    }
                }


            if (closest == null)
                return null;
            else return closest;
        }

        public static void RemoveObject(Vector2 position)
        {
            lock (_mut)
            {
                if (Current[position] == null)
                    return;
                else {
                    var obj = Current[position];    //save reference
                    Current.matrix[position.X, position.Y] = null;  //remove from map
                    var act = obj as Actor;
                    if (act != null)
                    {
                        if (act.PersonalThread != null && act.PersonalThread.ThreadState == ThreadState.Running)
                            act.PersonalThread.Abort();
                        if (ConcurrentMode)
                            Current.Actors.Remove(act);
                        else
                            Current.DoomedActors.Enqueue(act);
                    }
                    //raise Map Changed Event
                    if (MapChangedEvent != null)
                        MapChangedEvent(new Vector2[1] { position });
                }
            }
        }
    }
}
