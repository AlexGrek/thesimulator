﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator.Movement
{
    static class Movements
    {
        class RandomMovement : Way
        {
            /// <summary>
            /// Generates random movement
            /// </summary>
            /// <returns></returns>
            public override Vector2 NextStep()
            {
                Random r = Map.Current.Rand;
                var delta = r.Next(3) - 1;
                Vector2 movement;
                if (r.Next() % 2 > 0)
                    movement = new Vector2(0, delta);
                else
                    movement = new Vector2(delta, 0);
                return movement;
            }

            /// <summary>
            /// Tries to take any possible step for specified location
            /// </summary>
            /// <param name="me"></param>
            /// <returns></returns>
            public Vector2 MustNextStep(Vector2 me)
            {
                Random r = Map.Current.Rand;
                //select movement vector:
                if ((r.Next() & 1) > 0)
                {
                    //horizontal
                    
                }
                else
                {
                    //vertical

                }
                
                var delta = r.Next(3) - 1;
                Vector2 movement;
                if (r.Next() % 2 > 0)
                    movement = new Vector2(0, delta);
                else
                    movement = new Vector2(delta, 0);
                return movement;
            }

            /// <summary>
            /// Same as NextStep() here
            /// </summary>
            public override void PeekNextStep()
            {
                throw new NotImplementedException();
            }

            public override void Recalculate(Vector2 myPosition)
            {
                throw new NotImplementedException();
            }
        }
    }
}
