﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSimulator.Movement
{
    abstract class Way
    {
        /// <summary>
        /// Recalculate path if it is possible
        /// </summary>
        /// <param name="myPosition">My position</param>
        public abstract void Recalculate(Vector2 myPosition);

        /// <summary>
        /// Take the next step. Should be used only once to move. 
        /// Another cll of this method will return another step.
        /// </summary>
        /// <returns></returns>
        public abstract Vector2 NextStep();

        /// <summary>
        /// Get the next step without removing it from the step sequence.
        /// Can be useed to check availability of the movement without movement.
        /// </summary>
        public abstract void PeekNextStep();
    }
}
